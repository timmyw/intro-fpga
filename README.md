# FPGA Intro

Documenting my learning journey with FPGAs.

## IceBreaker

### Toolchain

Download the latest release from :

[fomu-toolchain](https://github.com/im-tomu/fomu-toolchain)
	
Extract it somewhere and add the `bin` directory to your `PATH`.

    git clone https://github.com/icebreaker-fpga/icebreaker-workshop.git
    cd icebreaker-workshop/stopwatch

### Links

+ https://github.com/icebreaker-fpga/icebreaker-workshop
